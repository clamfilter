/*++

Copyright (c) 1999-2002  Microsoft Corporation

Module Name:

    scanUser.c

Abstract:

    This file contains the implementation for the main function of the
    user application piece of scanner.  This function is responsible for
    actually scanning file contents.

Environment:

    User mode

--*/

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <winioctl.h>
#include <string.h>
#include <crtdbg.h>
#include <assert.h>
#include <fltuser.h>
#include "scanuk.h"
#include "scanuser.h"
#include <dontuse.h>

//
//  Default and Maximum number of threads.
//

#define SCANNER_DEFAULT_REQUEST_COUNT       5
#define SCANNER_DEFAULT_THREAD_COUNT        2
#define SCANNER_MAX_THREAD_COUNT            64

UCHAR FoulString[] = "foul";

//
//  Context passed to worker threads
//

typedef struct _SCANNER_THREAD_CONTEXT {

     HANDLE Port;
     HANDLE Completion;
     HANDLE queue_update_mutex: NULL;

} SCANNER_THREAD_CONTEXT, *PSCANNER_THREAD_CONTEXT;


VOID
Usage (
    VOID
    )
/*++

Routine Description

    Prints usage

Arguments

    None

Return Value

    None

--*/
{

    printf( "Connects to the scanner filter and scans buffers \n" );
    printf( "Usage: scanuser [requests per thread] [number of threads(1-64)]\n" );
}



#define DEFAULT_PORT "23123"

SOCKET
connect_socket(
    __in_bcount(dest_hostname_len) PUCHAR dest_hostname,
    __in SIZE_T (dest_hostname_len)
    )
{
    WSADATA wsaData;
    SOCKET connected_socket = INVALID_SOCKET;

    struct addrinfo *result = NULL, *ptr = NULL, hints;
    
    int iResult;

// Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        goto _bail;
    }


    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

// Resolve the server address and port
    iResult = getaddrinfo(dest_hostname, DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed: %d\n", iResult);
        goto _bail;
    }

// Attempt to connect to the first address returned by
// the call to getaddrinfo
    ptr = result;

// Create a SOCKET for connecting to server
    connected_socket = socket(
        ptr->ai_family, 
        ptr->ai_socktype, 
        ptr->ai_protocol);

    if (connected_socket == INVALID_SOCKET) {
        printf("Error at socket(): %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return INVALID_SOCKET;
    }
                          
    // Connect to server.
    iResult = connect( connected_socket, ptr->ai_addr, (int)ptr->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        closesocket(connected_socket);
        return INVALID_SOCKET;
    }

// Should really try the next address returned by getaddrinfo
// if the connect call failed
// But for this simple example we just free the resources
// returned by getaddrinfo and print an error message

    freeaddrinfo(result);

    if (connected_socket == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return INVALID_SOCKET;
    }

    _bail;
    if (connected_socket == INVALID_SOCKET) {
        WSACleanup();
        fprintf(stderr, "Failed initializing socket. %d\n", GetLastError() ); 
    }

    return connected_socket;
}

typedef enum _scan_status {
     SCAN_WAITING = 0,
     SCAN_DONE_CLEAN,
     SCAN_DONE_VIRUS,
     SCAN_DONE_NOTSURE
} e_scan_status, 
     *e_scan_status_ptr;

#define UPDATE_WAIT_PERIOD_MS	(5000)


BOOL
do_file_scan (
    __in_bcount(BufferSize) PUCHAR Buffer,
    __in ULONG BufferSize,
    __in PSCANNER_THREAD_CONTEXT p_context
    )
{
     BOOL res = FALSE;
     LPWSTR file_name = NULL;
     SIZE_T file_name_len;


     if (BufferSize < 1) {
          res = TRUE;
          goto _bail;
     }

     file_name = malloc( BufferSize );
     if (file_name == NULL) goto _bail;

     file_name_len = BufferSize / sizeof(WCHAR);


     if (WaitForSingleObject(p_context->queue_update_mutex, UPDATE_WAIT_PERIOD_MS) 
         != WAIT_OBJECT_0) {
          goto _bail;
     }
          
     __try { 

          
     } 

     __finally { 
          goto _bail;
     } 


_bail:    

     if (file_name) free(file_name);

     // Release ownership of the mutex object
     if ( !ReleaseMutex(p_context->queue_update_mutex) ) 
     { 
          fprintf(stderr, "Couldn't release mutex!");
     } 
     
     return res;
}


DWORD
ScannerWorker(
    __in PSCANNER_THREAD_CONTEXT p_context
    )
{
    PSCANNER_NOTIFICATION notification;
    SCANNER_REPLY_MESSAGE replyMessage;
    PSCANNER_MESSAGE message;
    LPOVERLAPPED pOvlp;
    BOOL result;
    DWORD outSize;
    HRESULT hr;
    ULONG_PTR key;


    while (TRUE) {

        //
        //  Poll for messages from the filter component to scan.
        //

        result = GetQueuedCompletionStatus( p_context->Completion, &outSize, &key, &pOvlp, INFINITE );

        //
        //  Obtain the message: note that the message we sent down via FltGetMessage() may NOT be
        //  the one dequeued off the completion queue: this is solely because there are multiple
        //  threads per single port handle. Any of the FilterGetMessage() issued messages can be
        //  completed in random order - and we will just dequeue a random one.
        //

        message = CONTAINING_RECORD( pOvlp, SCANNER_MESSAGE, Ovlp );

        if (!result) {

            //
            //  An error occured.
            //

            hr = HRESULT_FROM_WIN32( GetLastError() );
            break;
        }

        printf( "Received message, size %d\n", pOvlp->InternalHigh );

        notification = &message->Notification;

        assert(notification->BytesToScan <= SCANNER_READ_BUFFER_SIZE);
        __analysis_assume(notification->BytesToScan <= SCANNER_READ_BUFFER_SIZE);

        result = do_file_scan( 
            notification->Contents, 
            notification->BytesToScan, 
            p_context );

        replyMessage.ReplyHeader.Status = 0;
        replyMessage.ReplyHeader.MessageId = message->MessageHeader.MessageId;
        replyMessage.Reply.SafeToOpen = result == SCAN_DONE_CLEAN;

        printf( "Replying message, SafeToOpen: %d\n", replyMessage.Reply.SafeToOpen );

        hr = FilterReplyMessage( Context->Port,
                                 (PFILTER_REPLY_HEADER) &replyMessage,
                                 sizeof( replyMessage ) );

        if (SUCCEEDED( hr )) {

            printf( "Replied message\n" );

        } else {

            printf( "Scanner: Error replying message. Error = 0x%X\n", hr );
            break;
        }

        memset( &message->Ovlp, 0, sizeof( OVERLAPPED ) );

        hr = FilterGetMessage( Context->Port,
                               &message->MessageHeader,
                               FIELD_OFFSET( SCANNER_MESSAGE, Ovlp ),
                               &message->Ovlp );

        if (hr != HRESULT_FROM_WIN32( ERROR_IO_PENDING )) {

            break;
        }
    }

    if (!SUCCEEDED( hr )) {

        if (hr == HRESULT_FROM_WIN32( ERROR_INVALID_HANDLE )) {

            //
            //  Scanner port disconncted.
            //

            printf( "Scanner: Port is disconnected, probably due to scanner filter unloading.\n" );

        } else {

            printf( "Scanner: Unknown error occured. Error = 0x%X\n", hr );
        }
    }

    free( message );

    return hr;
}


int _cdecl
main (
    __in int argc,
    __in_ecount(argc) char *argv[]
    )
{
    DWORD requestCount = SCANNER_DEFAULT_REQUEST_COUNT;
    DWORD threadCount = SCANNER_DEFAULT_THREAD_COUNT;
    HANDLE threads[SCANNER_MAX_THREAD_COUNT];
    SCANNER_THREAD_CONTEXT context;
    HANDLE port, completion;
    PSCANNER_MESSAGE msg;
    DWORD threadId;
    DWORD retVal = 0;
    HRESULT hr;
    DWORD i, j;
    int res; // int _cdecl main
    HANDLE queue_update_mutex;


    queue_update_mutex = CreateMutex( 
         NULL,              // default security attributes
         FALSE,             // initially not owned
         NULL);             // unnamed mutex

    if (queue_update_mutex == NULL) 
    {
         fprintf(stderr, "CreateMutex error: %d\n", GetLastError());
         return 1;
    }


    //
    //  Check how many threads and per thread requests are desired.
    //

    if (argc > 1) {

        requestCount = atoi( argv[1] );

        if (argc > 2) {

            threadCount = atoi( argv[2] );
        }

        if (requestCount <= 0 || threadCount <= 0 || threadCount > 64) {

            Usage();
            res = 1;
            goto main_cleanup;
        }
    }

    //
    //  Open a commuication channel to the filter
    //

    printf( "Scanner: Connecting to the filter ...\n" );

    hr = FilterConnectCommunicationPort( ScannerPortName,
                                         0,
                                         NULL,
                                         0,
                                         NULL,
                                         &port );

    if (IS_ERROR( hr )) {

        printf( "ERROR: Connecting to filter port: 0x%08x\n", hr );
        res = 2;
        goto main_cleanup;
    }

    //
    //  Create a completion port to associate with this handle.
    //

    completion = CreateIoCompletionPort( port,
                                         NULL,
                                         0,
                                         threadCount );

    if (completion == NULL) {

        printf( "ERROR: Creating completion port: %d\n", GetLastError() );
        CloseHandle( port );
        res = 3;
        goto main_cleanup;
    }

    printf( "Scanner: Port = 0x%p Completion = 0x%p\n", port, completion );

    context.Port = port;
    context.Completion = completion;
    context.queue_update_mutex = queue_update_mutex;

    //
    //  Create specified number of threads.
    //

    for (i = 0; i < threadCount; i++) {

        threads[i] = CreateThread( NULL,
                                   0,
                                   ScannerWorker,
                                   &context,
                                   0,
                                   &threadId );

        if (threads[i] == NULL) {

            //
            //  Couldn't create thread.
            //

            hr = GetLastError();
            printf( "ERROR: Couldn't create thread: %d\n", hr );
            res = hr;
            goto main_cleanup;
        }

        for (j = 0; j < requestCount; j++) {

            //
            //  Allocate the message.
            //

            msg = malloc( sizeof( SCANNER_MESSAGE ) );

            if (msg == NULL) {

                hr = ERROR_NOT_ENOUGH_MEMORY;
                res = hr;
                goto main_cleanup;
            }

            memset( &msg->Ovlp, 0, sizeof( OVERLAPPED ) );

            //
            //  Request messages from the filter driver.
            //

            hr = FilterGetMessage( port,
                                   &msg->MessageHeader,
                                   FIELD_OFFSET( SCANNER_MESSAGE, Ovlp ),
                                   &msg->Ovlp );

            if (hr != HRESULT_FROM_WIN32( ERROR_IO_PENDING )) {

                free( msg );
                res = hr;
                goto main_cleanup;
            }
        }
    }

    hr = S_OK;

    WaitForMultipleObjects( i, threads, TRUE, INFINITE );

main_cleanup:

    printf( "Scanner:  All done. Result = 0x%08x\n", hr );

    CloseHandle( queue_update_mutex );

    CloseHandle( port );
    CloseHandle( completion );

    return hr;
}

